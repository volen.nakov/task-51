import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  setTimeout(() => {
    let notactive = document.querySelectorAll('.card:not(.active)')
    for (let i = 0; i < notactive.length; i++) {
      notactive[i].style['display'] = 'none'
    }
  }, 3000)
});
